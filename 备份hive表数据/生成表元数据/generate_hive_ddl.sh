#!/bin/bash

# 定义Hive数据库的名称
database_name="gmall"

# 定义要排除的表名
exclude_table="tab_name"

# 创建一个文件来存储创建表的SQL语句
output_file="/home/atguigu/create_tables.sql"
echo "-- 创建表的SQL语句" > $output_file

# 使用Hive的shell命令获取所有的表名
tables=$(hive -e "USE $database_name; SHOW TABLES;")

# 初始化一个字符串，用于存储所有的SHOW CREATE TABLE命令
commands="USE $database_name; "

# 遍历所有的表
for table in $tables; do
  # 如果表名不是要排除的表名，将SHOW CREATE TABLE命令添加到字符串中
  if [ "$table" != "$exclude_table" ]; then
    commands+="SHOW CREATE TABLE $table; "
  fi
done

# 使用hive -e命令获取所有的创建表的SQL语句
create_tables=$(hive -e "$commands")

# 将创建表的SQL语句写入到文件中
echo "$create_tables" >> $output_file

echo "创建表的SQL语句已经生成在文件：$output_file"
