#!/bin/bash

set -x

# 定义Hive数据库的名称
database_name="edu"

# 定义要排除的表名
exclude_table="tab_name"

# 使用Hive的shell命令获取所有的表名
tables=$(hive -e "USE $database_name; SHOW TABLES;")

# 初始化一个字符串，用于存储所有的MSCK REPAIR TABLE命令
commands="USE $database_name; "

# 遍历所有的表
for table in $tables; do
  # 将MSCK REPAIR TABLE命令添加到字符串中
  if [ "$table" != "$exclude_table" ]; then
    commands+="MSCK REPAIR TABLE $table; "
  fi
done

# 使用hive -e命令执行所有的MSCK REPAIR TABLE命令
hive -e "$commands"

echo "修复完毕所有表"
