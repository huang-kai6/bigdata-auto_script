#!/bin/bash

SERVER_NAME="hadoop102"
HIVE_SERVER2_PID=$(ssh "$SERVER_NAME" "ps -ef | grep -i '[o]rg.apache.hive.service.server.HiveServer2' | awk '{print \$2}'")

start_hiveServer2() {
    if [ -z "$HIVE_SERVER2_PID" ]; then
        echo "正在启动hiveServer2..."
        ssh "$SERVER_NAME" "nohup /opt/module/hive/bin/hive --service hiveserver2 >/dev/null 2>&1 &"
        echo "hiveServer2已启动"
    else
        echo "hiveServer2已经启动"
    fi
}

stop_hiveServer2() {
    if [ -n "$HIVE_SERVER2_PID" ]; then
        echo "正在关闭hiveServer2..."
        ssh "$SERVER_NAME" "kill $HIVE_SERVER2_PID"
        echo "hiveServer2已关闭"
    else
        echo "hiveServer2未启动"
    fi
}

case $1 in
    start)
        start_hiveServer2
        ;;
    stop)
        stop_hiveServer2
        ;;
    *)
        echo "用法: $0 {start|stop}"
        exit 1
        ;;
esac

