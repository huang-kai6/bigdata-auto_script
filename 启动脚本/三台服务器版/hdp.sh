#!/bin/bash

source /home/atguigu/bin/run_common.sh

HADOOP_HOME=/opt/module/hadoop

YARN_SERVER_NAME="hadoop103"

if [ $# -lt 1 ]
then
    echo "No Args Input..."
    exit ;
fi
case $1 in
"start")
        echo " =================== 启动 hadoop集群 ==================="

        echo " --------------- 启动 hdfs ---------------"
        ssh ${NameNode_servers[0]} "${HADOOP_HOME}/sbin/start-dfs.sh"
        echo " --------------- 启动 yarn ---------------"
        ssh ${YARN_SERVER_NAME} "${HADOOP_HOME}/sbin/start-yarn.sh"
        echo " --------------- 启动 historyserver ---------------"
        ssh ${NameNode_servers[0]} "${HADOOP_HOME}/bin/mapred --daemon start historyserver"
;;
"stop")
        echo " =================== 关闭 hadoop集群 ==================="

        echo " --------------- 关闭 historyserver ---------------"
        ssh ${NameNode_servers[0]} "${HADOOP_HOME}/bin/mapred --daemon stop historyserver"
        echo " --------------- 关闭 yarn ---------------"
        ssh ${YARN_SERVER_NAME} "${HADOOP_HOME}/sbin/stop-yarn.sh"
        echo " --------------- 关闭 hdfs ---------------"
        ssh ${NameNode_servers[0]} "${HADOOP_HOME}/sbin/stop-dfs.sh"
;;
*)
    echo "Input Args Error..."
;;
esac
