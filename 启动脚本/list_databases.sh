#!/bin/bash

# 定义MySQL登录信息
MYSQL_USER="root"
MYSQL_PASS="000000"

# 进入MySQL客户端并接收返回结果
query_result=$(mysql -u "${MYSQL_USER}" -p"${MYSQL_PASS}" <<EOF
SHOW DATABASES;
EOF
)

# 处理查询结果并构建数据库列表
database_list=()
while read -r line; do
    # 将结果添加到数组中（排除第一行的表头）
    if [ "$line" != "Database" ]; then
        database_list+=("$line")
    fi
done <<< "$query_result"

# 输出数据库列表（可选）
echo "Database List:"
for db in "${database_list[@]}"; do
    echo "- $db"
done

# 在这里可以继续处理数据库列表，根据需要进行其他操作
# 例如：遍历数据库列表并执行其他操作

# 示例：遍历数据库列表并输出表格数目
for db in "${database_list[@]}"; do
    table_count=$(mysql -u "${MYSQL_USER}" -p"${MYSQL_PASS}" -D "$db" -e "SHOW TABLES;" 2>/dev/null | wc -l)
    echo "Database '$db' contains $table_count tables."
done

echo "操作成功"
