#! /bin/bash

# 确保脚本在遇到错误时立即退出
set -e
set -o pipefail

source /home/atguigu/bin/run_common.sh

case $1 in
"start"){
    for server in "${kafka_servers[@]}"; do
        echo " --------启动 $server Kafka-------"
        ssh $server "/opt/module/kafka/bin/kafka-server-start.sh -daemon /opt/module/kafka/config/server.properties"
    done
};;
"stop"){
    for server in "${kafka_servers[@]}"; do
        echo " --------停止 $server Kafka-------"
        ssh $server "/opt/module/kafka/bin/kafka-server-stop.sh "
    done
};;
esac
