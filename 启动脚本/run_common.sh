# 确保脚本在遇到错误时立即退出
set -x
set -e
set -o pipefail

# 用你的MySQL用户名和密码替换这里的值
mysql_user="root"
mysql_password="000000"
DATABASE_NAME="gmall"

# 定义服务器列表、密码和用户名等变量
servers=(
  "hadoop100"
  "hadoop101"
  "hadoop102"
  "hadoop103"
  "hadoop104"
)

#安装zookeeper的服务器
zookeeper_servers=(
  "hadoop102"
  "hadoop103"
  "hadoop104"
)

#安装kafka的服务器
kafka_servers=(
  "hadoop102"
  "hadoop103"
  "hadoop104"
)

#安装NameNode的服务器
NameNode_servers=(
  "hadoop100"
  "hadoop101"
)

#安装MySQL的服务器
MySQL_servers=(
  "hadoop102"
)