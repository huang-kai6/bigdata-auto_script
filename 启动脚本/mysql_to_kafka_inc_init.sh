#!/bin/bash

source ./run_common.sh

# 该脚本的作用是初始化所有的增量表，只需执行一次

MAXWELL_HOME=/opt/module/maxwell

import_data() {
    $MAXWELL_HOME/bin/maxwell-bootstrap --database ${DATABASE_NAME} --table $1 --config $MAXWELL_HOME/config.properties
}

#=================================

input_param="$1"

if [[ -z "$input_param" ]]; then
  echo "请输入参数"
  exit 1
fi

#=================================

#定义一个数组
table_array=()

# 使用MySQL命令行工具获取所有表名，并将结果保存到数组中
table_list=$(ssh ${MySQL_servers[0]} "mysql -u"$mysql_user" -p"$mysql_password" -D"$DATABASE_NAME" -Bse 'show tables;'")
table_array=($table_list)

# 输出数组中的表名
for table in "${table_array[@]}"; do
  echo "从数据库获取的表名为： $table"
done

#=================================

found=0

if [[ "$input_param" == "all" ]]; then
  found=1
  for file in "${table_array[@]}"; do
	
	import_data ${file}
    
  done
else
  for file in "${table_array[@]}"; do
    if [[ "$file" == "$input_param" ]]; then
      found=1
      
	  import_data ${file}
	  
      break
    fi
  done
fi

if [[ $found -eq 0 ]]; then
  echo "参数错误"
  exit 1
fi

echo "执行成功"