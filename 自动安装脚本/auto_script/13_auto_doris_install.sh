#!/bin/bash

source ./common.sh

#设置系统最大打开文件句柄数(注意这里的*不要去掉)
for server in "${doris_servers[@]}"; do

	echo "=====请手动执行以下命令：====="
	echo "设置系统最大打开文件句柄数(注意这里的*不要去掉)："
	echo "sudo vim /etc/security/limits.conf

			* soft nofile 65536
			* hard nofile 131072
			* soft nproc 131072
			* hard nproc 131072"
			
	echo "========重启生效!!!========"
	echo "========重启生效!!!========"
	echo "========重启生效!!!========"
	
  # 设置最大虚拟块的大小
  max_map_count_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/vm.max_map_count/{print NR; exit}' /etc/sysctl.conf")
  
  if [[ -n "${max_map_count_line_number}" ]]; then
	  # 使用 http_port_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${max_map_count_line_number}d\" /etc/sysctl.conf"
  fi
  
  LOG_DIR_TEMP="vm.max_map_count=2000000"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i \"${max_map_count_line_number}i${LOG_DIR_TEMP}\" /etc/sysctl.conf"
	
  # 追加新的配置
  #for line in "${env_variables[@]}"; do
    #sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo '${line}' | sudo tee -a /etc/profile.d/my_env.sh"
  #done
done

# 复制安装包
for server in "${doris_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mkdir -p ${software_directory}doris"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${doris_fe_package_path}  ${new_user}@${server}:${software_directory}doris/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${doris_be_package_path}  ${new_user}@${server}:${software_directory}doris/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${doris_dependencies_package_path}  ${new_user}@${server}:${software_directory}doris/"
  
done

# 删除已存在的目录
for server in "${doris_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo rm -rf ${module_path}/doris*"
  
done

# 解压缩安装包
for server in "${doris_servers[@]}"; do
  
  echo "开始解压 ${server}"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "mkdir -p ${module_path}/doris"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -xf ${doris_fe_package_path} -C ${module_path}/doris/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -xf ${doris_be_package_path} -C ${module_path}/doris/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -xf ${doris_dependencies_package_path} -C ${module_path}/doris/"
  
done

#修改解压后的文件名称
for server in "${doris_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/doris/apache-doris-fe-1.2.4.1-bin-x86_64 ${module_path}/doris/fe"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/doris/apache-doris-be-1.2.4.1-bin-x86_64 ${module_path}/doris/be"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/doris/apache-doris-dependencies-1.2.4.1-bin-x86_64 ${module_path}/doris/dependencies"
  
done

#3.安装其他依赖（java udf 函数）
for server in "${doris_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "cp /opt/module/doris/dependencies/java-udf-jar-with-dependencies.jar /opt/module/doris/be/lib"
  
done

#修改FE配置文件
for server in "${doris_servers[@]}"; do
  
  # web 页面访问端口
  http_port_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/http_port/{print NR; exit}' /opt/module/doris/fe/conf/fe.conf")
  
  if [[ -n "${http_port_line_number}" ]]; then
	  # 使用 http_port_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${http_port_line_number}d\" /opt/module/doris/fe/conf/fe.conf"
  fi
  
  LOG_DIR_TEMP="http_port = 7030"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i \"${http_port_line_number}i${LOG_DIR_TEMP}\" /opt/module/doris/fe/conf/fe.conf"
  
  #修改绑定 ip
  priority_networks_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/priority_networks/{print NR; exit}' /opt/module/doris/fe/conf/fe.conf")
  
  if [[ -n "${priority_networks_line_number}" ]]; then
	  # 使用 http_port_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${priority_networks_line_number}d\" /opt/module/doris/fe/conf/fe.conf"
  fi
  
  LOG_DIR_TEMP="priority_networks = ${doris_priority_networks}/16"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i \"${priority_networks_line_number}i${LOG_DIR_TEMP}\" /opt/module/doris/fe/conf/fe.conf"
  
done

#启动FE
for server in "${doris_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "/opt/module/doris/fe/bin/start_fe.sh --daemon"
  
  sleep 30
  
done



#修改BE配置文件
for server in "${doris_servers[@]}"; do
  
  # web 页面访问端口
  http_port_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/webserver_port/{print NR; exit}' /opt/module/doris/be/conf/be.conf")
  
  if [[ -n "${http_port_line_number}" ]]; then
	  # 使用 http_port_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${http_port_line_number}d\" /opt/module/doris/be/conf/be.conf"
  fi
  
  LOG_DIR_TEMP="webserver_port = 7040"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i \"${http_port_line_number}i${LOG_DIR_TEMP}\" /opt/module/doris/be/conf/be.conf"
  
  #修改绑定 ip
  priority_networks_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/priority_networks/{print NR; exit}' /opt/module/doris/be/conf/be.conf")
  
  if [[ -n "${priority_networks_line_number}" ]]; then
	  # 使用 http_port_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${priority_networks_line_number}d\" /opt/module/doris/be/conf/be.conf"
  fi
  
  LOG_DIR_TEMP="priority_networks = ${doris_priority_networks}/16"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i \"${priority_networks_line_number}i${LOG_DIR_TEMP}\" /opt/module/doris/be/conf/be.conf"
  
done

#修改FE密码
for server in "${doris_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "
aaa=\$(mysql -h${server} -P9030 -u${doris_user} <<EOF
SET PASSWORD FOR 'root' = PASSWORD(\"${doris_password}\");
EOF
)"
  
done

#添加 BE
for server in "${doris_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "
aaa=\$(mysql -h${server} -P9030 -u${doris_user} -p${doris_password} <<EOF
ALTER SYSTEM ADD BACKEND \"${server}:9050\";
EOF
)"
  
done

#启动 BE
for server in "${doris_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "/opt/module/doris/be/bin/start_be.sh --daemon"
  
done

#重启doris
for server in "${doris_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "/opt/module/doris/fe/bin/stop_fe.sh --daemon"
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "/opt/module/doris/fe/bin/start_fe.sh --daemon"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "/opt/module/doris/be/bin/stop_be.sh --daemon"
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "/opt/module/doris/be/bin/start_be.sh --daemon"
  
done

echo "doris 安装成功"
