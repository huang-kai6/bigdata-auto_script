#!/bin/bash

source ./common.sh

# 配置环境变量
env_variables=(
  "#HBASE_HOME"
  "export HBASE_HOME=${module_path}/hbase"
  "export PATH=\$PATH:\$HBASE_HOME/bin"
)

# 复制安装包
for server in "${hbase_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${hbase_package_path}  ${new_user}@${server}:${software_directory}"
  
done

# 删除已存在的目录
for server in "${hbase_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo rm -rf ${module_path}/hbase*"
  
done

# 解压缩安装包
for server in "${hbase_servers[@]}"; do
  
  echo "开始解压 ${server}"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -zxf ${hbase_package_path} -C ${module_path}/"
  
done

#修改解压后的文件名称
for server in "${hbase_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/hbase-2.4.11 ${module_path}/hbase"
  
done

# 判断my_env.sh文件是否存在，如果不存在，新建
for server in "${hbase_servers[@]}"; do
  
  echo "开始判断my_env.sh文件是否存在"
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo touch /etc/profile.d/my_env.sh"
  echo "判断my_env.sh文件是否存在完成"
  
done

# 删除原有的配置
for server in "${hbase_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i '/HBASE_HOME/d' /etc/profile.d/my_env.sh"
  
done

# 追加新的配置
for server in "${hbase_servers[@]}"; do
  
  for line in "${env_variables[@]}"; do
    sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo '${line}' | sudo tee -a /etc/profile.d/my_env.sh"
  done
  
done

# 刷新环境变量
for server in "${hbase_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "source /etc/profile"
  
done

#hbase-env.sh修改内容，可以添加到最后：
for server in "${hbase_servers[@]}"; do
	
	sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo 'export HBASE_MANAGES_ZK=false' | sudo tee -a /opt/module/hbase/conf/hbase-env.sh"
	
done

#hbase-site.xml修改内容：

echo "hbase-site.xml修改内容："

echo "<?xml version=\"1.0\"?>
<?xml-stylesheet type=\"text/xsl\" href=\"configuration.xsl\"?>

<configuration>

  <property>
    <name>hbase.zookeeper.quorum</name>
    <value>hadoop102,hadoop103,hadoop104</value>
  </property>

  <property>
    <name>hbase.rootdir</name>
    <value>hdfs://hadoop102:8020/hbase</value>
  </property>


  <property>
    <name>hbase.cluster.distributed</name>
    <value>true</value>
  </property>

  <property>
    <name>hbase.wal.provider</name>
    <value>filesystem</value>
  </property>
</configuration>"

#regionservers
for server in "${hbase_servers[@]}"; do
	
	sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo 'hadoop102' | sudo tee -a /opt/module/hbase/conf/regionservers"
	
done

#解决HBase和Hadoop的log4j兼容性问题，修改HBase的jar包，使用Hadoop的jar包
for server in "${hbase_servers[@]}"; do
	
	sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv /opt/module/hbase/lib/client-facing-thirdparty/slf4j-reload4j-1.7.33.jar /opt/module/hbase/lib/client-facing-thirdparty/slf4j-reload4j-1.7.33.jar.bak"
	
done

#HBase服务的启动
#for server in "${hbase_servers[@]}"; do
#	
#	sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "start-hbase.sh"
#	
#done

echo "hbase 安装成功"

