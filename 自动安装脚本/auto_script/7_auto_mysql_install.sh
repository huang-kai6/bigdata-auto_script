#!/bin/bash

source ./common.sh

echo "注意：使用我的install_mysql.sh"

#执行mysql安装脚本
for server in "${MySQL_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/install_mysql.sh ${new_user}@${server}:${software_directory}mysql/"

  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "chmod 777 ${software_directory}mysql/install_mysql.sh"

  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "cd ${software_directory}mysql/; ${software_directory}mysql/install_mysql.sh"
  
done

#有时候这些命令不成功，再执行一遍
for server in "${MySQL_servers[@]}"; do

  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"alter user 'root'@'%' identified with mysql_native_password by '000000';\""
  
  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"flush privileges;\""
  
  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "systemctl restart mysqld"
  
done

echo "mysql安装成功"